var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var config = require('./');
var pkg = require('../package.json');
var env = process.env.NODE_ENV || 'development';

module.exports = function (app) {

    // static files middleware
    app.use(express.static(config.root + '/public'));

    // parsers
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());

    app.use(function (req, res, next) {
        req.fullOriginalUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

        // expose package.json and env to views
        res.locals.pkg = pkg;
        res.locals.env = env;
        next();
    });
};