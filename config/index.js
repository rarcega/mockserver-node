var path = require('path');
var _ = require('lodash');

var development = require('./env/development');
var test = require('./env/test');
var production = require('./env/production');

var defaults = {
    root: path.join(__dirname, '..'),

    request: {
        includeHeaders: ['x-mock-scenario', 'x-mock-request-sequence'], // use lowercase as express converts
        excludeQueryString: ['source', 'version']
    }
};

module.exports = {
    development : _.assign(defaults, development),
    test        : _.assign(defaults, test),
    production  : _.assign(defaults, production)
}[process.env.NODE_ENV || 'development'];