var controllers = require('../app/controllers');

module.exports = function (app) {

    app.use(function(req, res, next) {
        if ( ! ( 'x-mock-scenario' in req.headers ) ) {
            return next(new Error('Missing x-mock-scenario header'));
        }

        next();
    });

    // routes
    app.all('*', controllers.all);

    // error handling
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        res
            .status(err.status || 200)
            .set('Content-Type', 'text/xml;charset=UTF-8')
            .send('<?xml version="1.0"?><amtd><result>FAIL</result><error>' + err.message + '</error></amtd>');
    });
};