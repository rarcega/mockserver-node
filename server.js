console.log('NODE_ENV=' + (process.env.NODE_ENV || 'development'));

var config = require('./config');
var express = require('express');
var port = process.env.PORT || 3000;
var app = express();

module.exports = app;

require('./config/express')(app);
require('./config/routes')(app);

require('./app/db').connect(config.mongo.url, listen);

function listen() {
    if (app.get('env') === 'test') {
        return;
    }

    app.listen(port);

    console.log('Express app started on port ' + port);
}

	
