var MongoClient = require('mongodb').MongoClient;

var _db;


exports.connect = function (url, callback) {
    if (_db) {
        return callback();
    }

    MongoClient.connect(url, function (err, db) {
        if (err) {
            return callback(err);
        }

        console.log('Connected to Mongo server on ' + db.serverConfig.host + ':' + db.serverConfig.port);

        _db = db;
        callback();
    });
};

exports.get = function () {
    return _db;
};

exports.close = function (callback) {
    if (_db) {
        _db.close(function (err) {
            _.db = null;
            callback(err)
        });
    }
};