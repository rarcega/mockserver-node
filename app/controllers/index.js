var db = require('../db');
var queryUtil = require('../util/QueryUtil');

exports.all = function (req, res, next) {

    var query  = queryUtil.getResReqQuery(req, res);
    var fields = { fields: { 'response.status': 1, 'response.content': 1 } };
    
    db.get().collection( req.headers['x-mock-scenario'] )
        .find(query, fields)
        .limit(1)
        .next(function(err, doc) {
            if (err) return next(err);

            if (doc == null) {
                return next(new Error('Could not find matching document: ' + JSON.stringify(query, null, 2)));
            }

            res
                .status(doc.response.status)
                .set('Content-Type', doc.response.content.mimeType)
                .send(doc.response.content.text);
    });
};




