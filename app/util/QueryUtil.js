var config = require('../../config');
var _ = require('lodash');

module.exports = {
    getResReqQuery: getResReqQuery
};

function getResReqQuery(req, res) {
    return {
        '$and': [
            { 'request.method' : req.method },
            { 'request.url'    : { $regex: req.originalUrl.split(/[;?]/g)[0] } }
        ]
            .concat(
                  getHeaderFilters(req)
                , getQueryStringFilters(req)
                , getPostDataFilters(req)
                //, getCookieFilters(req)
            )
    };
}

function getHeaderFilters(req) {
    return _.chain(req.headers)
        .pickBy(function(val, name) {
            return config.request.includeHeaders.indexOf(name) > -1;
        })
        .map(function(val, name) {
            return {
                'request.headers': {
                $elemMatch : {
                    name: { $regex: name, $options: 'i' },
                    value:  val
                }
                }
            };
        })
        .value();
}

function getQueryStringFilters (req) {
    return _.chain(req.query)
        .pickBy(function(val, name) {
            return config.request.excludeQueryString.indexOf(name) == -1;
        })
        .map(function(val, name) {
            return {
                'request.queryString': { $elemMatch : {
                    name: name,
                    value:  val
                }}
            };
        })
        .value();
}

function getPostDataFilters(req) {
    if (req.method != 'POST' || req.method != 'PUT') {
        return [];
    }

    return _.chain(req.body)
        .map(function(val, name) {
            return {
                'request.postData.params': { $elemMatch : {
                    name: name,
                    value: val
                }}
            };
        })
        .value();
}


function getCookieFilters(req) {
    return _.chain(req.cookies)
        .map(function(val, name) {
            return {
                'request.cookies': { $elemMatch : {
                    name: name,
                    value:  val
                }}
            };
        })
        .value();
}