# mockserver-node

A simple server for mocking HTTP request/response pairs.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. See deployment for notes on how to deploy the project on a live system.

### Install
Prerequisites: Node.js

Install the project dependencies using npm:

`
$ npm install
`

### Usage

To start the mock server:

```
$ node server.js
```


### Deployment


#### Loading data from Charles Proxy export file into MongoDB

Copy the Charles proxy export trace file into the data directory.

Next run the loader.js script to insert the documents into the collection:

`
node data/loader.js <filename.json>
`
