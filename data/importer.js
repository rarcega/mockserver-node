var exec = require('child_process').exec;
var fs = require('fs');
var os = require('os');
var path = require('path');

var filename = process.argv[2];
var json = require('./' + filename);
var entries = json.log.entries;

importEntries(filename, entries);

function importEntries(filename, entries) {
    var entriesFile = createEntriesFile(filename, entries);

    var cmd = 'mongoimport --db mockserver --collection reqres --type json --file ' + entriesFile + ' --jsonArray';

    exec(cmd, function callback(err, stdout, stderr){
        fs.unlinkSync(entriesFile);

        if (err) {
            console.log('Child process exited with error code', err.code);
        }

        if (stdout) console.log(stdout);
        if (stderr) console.log(stderr);

        process.exit();
    });

}

function createEntriesFile(filename, entries) {
    var entriesFile = os.tmpdir() + '/';

    filename = path.basename(filename);

    if (filename.indexOf('.') > -1) {
        var filenameParts = filename.split('.');
        entriesFile += filenameParts[0] + '_entries.' + filenameParts[1];
    } else {
        entriesFile += filename + '_entries.json';
    }

    fs.writeFileSync(entriesFile, JSON.stringify(entries, null, 4));

    return entriesFile;
}