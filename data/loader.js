/**
 * Inserts an exported trace file from Charles Proxy into MongoDB.
 *
 * Documents are inserted into collections named by their corresponding
 * x-mock-scenario header values.
 */

if (process.argv.length < 3) {
    return console.error('Please specify the file to be loaded e.g. node loader.js <filename>');
}

var filename = process.argv[2];
var json = require('./' + filename);
var entries = json.log.entries;

var _ = require('lodash');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');

var scenariosToDocuments = {};
var docsMissingHeader = [];

/**
 * Groups the request/response pairs into a map where <k,v>
 *
 * scenario name  => list of request/responses pairs
 */
for (var i = 0; i < entries.length; i++) {
    var foundHeader = false;
    var headers = entries[i].request.headers;

    for (var j = 0; j < headers.length; j++) {
        if (headers[j].name.toLowerCase() == 'x-mock-scenario' && ! _.isEmpty(headers[j].value) ) {

            if ( ! ( headers[j].value in scenariosToDocuments) ) {
                scenariosToDocuments[headers[j].value] = [];
            }
            scenariosToDocuments[headers[j].value].push(entries[i]);

            foundHeader = true;
            break;
        }
    }

    if ( ! foundHeader) {
        docsMissingHeader.push(entries[i]);
    }
}


/**
 * Bulk inserts data into MongoDB.
 *
 * Documents are batched by collection.
 */
MongoClient.connect(config.mongo.url, function(err, db) {
    if (err) {
        return console.err(err);
    }

    console.log('Connected to Mongo server on ' + db.serverConfig.host + ':' + db.serverConfig.port);

    var batches = 0;
    var nInserted = 0;

    for (var scenario in scenariosToDocuments) {
        if (scenariosToDocuments.hasOwnProperty(scenario)) {
            var col = db.collection(scenario);

            var batch = col.initializeUnorderedBulkOp();

            for (var i = 0; i < scenariosToDocuments[scenario].length; i++) {
                var doc = scenariosToDocuments[scenario][i];
                batch.insert(doc);
            }

            if (batch.length) {
                batches++;

                // bulk insert the documents
                batch.execute(function (err, result) {
                    if (err) console.dir(err);
                    if (result) nInserted += result.nInserted;

                    // once all batches are inserted
                    if (--batches == 0) {
                        if (docsMissingHeader.length) {
                            console.warn('Failed to insert ' + docsMissingHeader.length + ' documents');
                            require('fs').writeFileSync('./failed.json', JSON.stringify(docsMissingHeader, null, 4));
                        }
                        console.log('Inserted ' + nInserted + ' documents into [' + _.keys(scenariosToDocuments) + ']');

                        db.close();
                        process.exit();
                    }
                });
            }
        }
    }
});




